#!/bin/bash

if [[ "$(docker images -q mosquitto-docker-rpi 2> /dev/null)" == "" ]]; then
	echo "Container doesn't exist. Building it first."
	echo ""
	./build.sh
fi

# Include "--volume /path/to/persistence:mosquitto_data" if you want to ensure message persistence at shutdown.

sudo docker run -d \
        --name=mosquitto \
	--volume /path/to/persistence:/mosquitto_data \
	-e MQTT_HOST=your_main_mqtt \
	-e MQTT_CLIENTID=your_id \
	-e MQTT_USERNAME=your_username \
	-e MQTT_PASSWORD=your_password \
	-e MQTT_TOPIC=topic_to_be_synced \
	-p 1883:1883 \
	mosquitto-docker-rpi
