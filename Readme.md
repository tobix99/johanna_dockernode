# Docker & Docker-Compose Installations Skript
## Starten
Mit dem Befehl ```sudo ./start.sh``` kann das Skript gestartet werden. Vorher aber unbedigt im ```docker-compose.yml``` in Zeile 28-31 die Passwörter und Benutzernamen für die MySQL-Datenbank setzen!
Desweiteren ist es wichtig im ```mosquitto.userfile``` im ordner ```MQTT``` Benutzername und Passwort wie folgt zu setzen:

Pro Zeile ein Benutzernamen und Passwort:
- BENUTZERNAME:PASSWORT

Danach kann das Skript wie oben beschrieben mit ```sudo ./start.sh``` ausgeführt werden. Es kann vorkommen, dass docker container nicht starten (das bekommst du mit), weil ich es nicht auf einem RPi ausgetetstet habe. Geb mir dann einfach bescheid!

## Services
Folgende Services sind dadurch erreichbar:
### NodeRed
NodeRed ist eine Grafische Programmiersprache, mit der einfach auch Sachen debugged werden können. Es können beispielsweise dort MQTT Nachrichten empfangen und angezeigt werden.
Zu erreich ist NodeRed unter ```[IP/Localhost]:1880```

### MySQL
MySQL ist unter ```[IP/Localhost]:3306``` zu erreichen.

### PhpMyAdmin
PhpMyAdmin ist ein einfaches Tool um Datenbanken zu verwalten und unter ```[IP/Localhost]:8081``` zu erreichen.

### MQTT
MQTT ist unter ```[IP/Localhost]:1883``` zu erreichen.