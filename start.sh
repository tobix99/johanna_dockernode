#!/bin/bash
if ! [ $(id -u) = 0 ]; then
   echo "Bitte mit Sudo-Rechten ausführen: 'sudo ./start.sh'"
   exit 1
fi

echo "Heeeeeeeeeeey was geeeeht. Folgendes wird nun passieren: Ich werde docker und docker-compose auf deinem Raspberry-Pi installieren und den dockerstack mit NodeRed, MQTT-Brocker und der MySQL Datenbank, sowie PhpMyAdmin starten.
Für weitere Infos zu Ports einfach in die 'docker-compose.yml' Datei schauen bzw mich fragen!"
echo "Hast du auch vorher alles in der 'docker-compose.yml' Datei angepasst, wie Passwörter und Username? Wenn nicht CTLR+C jetzt drücken!"
read -p "Ansonsten Enter drücken um fort zu fahren."

echo "Installiere Docker"
curl -fsSL https://get.docker.com | bash

sudo usermod -aG docker $SUDO_USER

echo "Installiere docker-compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "Gebe docker-compose Version aus"
docker-compose --version

echo "Starte die Docker-Container"
docker-compose up -d

echo "Damit alles richtig funktioniert einmal bitte noch neustarten!"
echo "Alles bereit. Viel Spaß :)"

